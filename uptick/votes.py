import click
from dxpchain.account import Account
from dxpchain.amount import Amount
from dxpchain.vesting import Vesting
from .decorators import online
from .main import main, config
from .ui import print_table


class Vote:
    BLOCKPRODUCER = "blockproducer"
    DXPCORE = "dxpcore"
    BENEFACTOR = "benefactor"

    @staticmethod
    def types():
        return [Vote.BLOCKPRODUCER, Vote.DXPCORE, Vote.BENEFACTOR]

    @staticmethod
    def vote_type_from_id(id):
        if id[:4] == "1.6.":
            return Vote.BLOCKPRODUCER
        elif id[:4] == "1.5.":
            return Vote.DXPCORE
        elif id[:5] == "1.14.":
            return Vote.BENEFACTOR


@main.command()
@click.argument("account", default=config["default_account"])
@click.option("--type", default=Vote.types())
@click.pass_context
@online
def votes(ctx, account, type):
    """ List accounts vesting balances
    """
    if not isinstance(type, (list, tuple)):
        type = [type]
    account = Account(account, full=True)
    ret = {key: list() for key in Vote.types()}
    for vote in account["votes"]:
        t = Vote.vote_type_from_id(vote["id"])
        ret[t].append(vote)

    t = [["id", "url", "account"]]
    for vote in ret["dxpcore"]:
        t.append(
            [vote["id"], vote["url"], Account(vote["dxpcore_member_account"])["name"]]
        )

    if "dxpcore" in type:
        t = [["id", "url", "account", "votes"]]
        for vote in ret["dxpcore"]:
            t.append(
                [
                    vote["id"],
                    vote["url"],
                    Account(vote["dxpcore_member_account"])["name"],
                    str(Amount({"amount": vote["total_votes"], "asset_id": "1.3.0"})),
                ]
            )
        print_table(t)

    if "blockproducer" in type:
        t = [
            [
                "id",
                "account",
                "url",
                "votes",
                "last_confirmed_block_num",
                "total_missed",
                "westing",
            ]
        ]
        for vote in ret["blockproducer"]:
            t.append(
                [
                    vote["id"],
                    Account(vote["blockproducer_account"])["name"],
                    vote["url"],
                    str(Amount({"amount": vote["total_votes"], "asset_id": "1.3.0"})),
                    vote["last_confirmed_block_num"],
                    vote["total_missed"],
                    str(Vesting(vote.get("pay_vb")).claimable)
                    if vote.get("pay_vb")
                    else "",
                ]
            )
        print_table(t)

    if "benefactor" in type:
        t = [["id", "name/url", "daily_pay", "votes", "time", "account"]]
        for vote in ret["benefactor"]:
            votes = Amount({"amount": vote["total_votes_for"], "asset_id": "1.3.0"})
            amount = Amount({"amount": vote["daily_pay"], "asset_id": "1.3.0"})
            t.append(
                [
                    vote["id"],
                    "{name}\n{url}".format(**vote),
                    str(amount),
                    str(votes),
                    "{work_begin_date}\n-\n{work_end_date}".format(**vote),
                    str(Account(vote["Benefactor_account"])["name"]),
                ]
            )
        print_table(t)
