import os
import yaml
import click
import logging
from dxpchain import DxpChain
from dxpchain.exceptions import WrongMasterPasswordException
from dxpchain.instance import set_shared_dxpchain_instance
from functools import update_wrapper
from .ui import print_message

log = logging.getLogger(__name__)


def verbose(f):
    """ Add verbose flags and add logging handlers
    """

    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        global log
        verbosity = ["critical", "error", "warn", "info", "debug"][
            int(min(ctx.obj.get("verbose", 0), 4))
        ]
        log.setLevel(getattr(logging, verbosity.upper()))
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
        ch = logging.StreamHandler()
        ch.setLevel(getattr(logging, verbosity.upper()))
        ch.setFormatter(formatter)
        log.addHandler(ch)

        # GrapheneAPI logging
        if ctx.obj.get("verbose", 0) > 4:
            verbosity = ["critical", "error", "warn", "info", "debug"][
                int(min(ctx.obj.get("verbose", 4) - 4, 4))
            ]
            log = logging.getLogger("grapheneapi")
            log.setLevel(getattr(logging, verbosity.upper()))
            log.addHandler(ch)

        if ctx.obj.get("verbose", 0) > 8:
            verbosity = ["critical", "error", "warn", "info", "debug"][
                int(min(ctx.obj.get("verbose", 8) - 8, 4))
            ]
            log = logging.getLogger("graphenebase")
            log.setLevel(getattr(logging, verbosity.upper()))
            log.addHandler(ch)

        return ctx.invoke(f, *args, **kwargs)

    return update_wrapper(new_func, f)


def offline(f):
    """ This decorator allows you to access ``ctx.dxpchain`` which is
        an instance of DxpChain with ``offline=True``.
    """

    @click.pass_context
    @verbose
    def new_func(ctx, *args, **kwargs):
        ctx.obj["offline"] = True
        ctx.dxpchain = DxpChain(**ctx.obj)
        ctx.blockchain = ctx.dxpchain
        ctx.dxpchain.set_shared_instance()
        return ctx.invoke(f, *args, **kwargs)

    return update_wrapper(new_func, f)


def customchain(**kwargsChain):
    """ This decorator allows you to access ``ctx.dxpchain`` which is
        an instance of DxpChain. But in contrast to @chain, this is a
        decorator that expects parameters that are directed right to
        ``DxpChain()``.

        ... code-block::python

                @main.command()
                @click.option("--benefactor", default=None)
                @click.pass_context
                @customchain(foo="bar")
                @unlock
                def list(ctx, benefactor):
                   print(ctx.obj)

    """

    def wrap(f):
        @click.pass_context
        @verbose
        def new_func(ctx, *args, **kwargs):
            newoptions = ctx.obj
            newoptions.update(kwargsChain)
            ctx.dxpchain = DxpChain(**newoptions)
            ctx.blockchain = ctx.dxpchain
            set_shared_dxpchain_instance(ctx.dxpchain)
            return ctx.invoke(f, *args, **kwargs)

        return update_wrapper(new_func, f)

    return wrap


def chain(f):
    """ This decorator allows you to access ``ctx.dxpchain`` which is
        an instance of DxpChain.
    """

    @click.pass_context
    @verbose
    def new_func(ctx, *args, **kwargs):
        ctx.dxpchain = DxpChain(**ctx.obj)
        ctx.blockchain = ctx.dxpchain
        set_shared_dxpchain_instance(ctx.dxpchain)
        return ctx.invoke(f, *args, **kwargs)

    return update_wrapper(new_func, f)


def unlock(f):
    """ This decorator will unlock the wallet by either asking for a
        passphrase or taking the environmental variable ``UNLOCK``
    """

    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        if not ctx.obj.get("unsigned", False):
            if ctx.dxpchain.wallet.created():
                while True:
                    if "UNLOCK" in os.environ:
                        pwd = os.environ["UNLOCK"]
                    else:
                        pwd = click.prompt("Current Wallet Passphrase", hide_input=True)
                    try:
                        ctx.dxpchain.wallet.unlock(pwd)
                    except WrongMasterPasswordException:
                        print_message("Incorrect Wallet passphrase!", "error")
                        continue
                    break
            else:
                print_message("No wallet installed yet. Creating ...", "warning")
                if "UNLOCK" in os.environ:
                    pwd = os.environ["UNLOCK"]
                else:
                    pwd = click.prompt(
                        "Wallet Encryption Passphrase",
                        hide_input=True,
                        confirmation_prompt=True,
                    )
                ctx.dxpchain.wallet.create(pwd)
        return ctx.invoke(f, *args, **kwargs)

    return update_wrapper(new_func, f)


def configfile(f):
    """ This decorator will parse a configuration file in YAML format
        and store the dictionary in ``ctx.config``
    """

    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        ctx.config = yaml.load(open(ctx.obj["configfile"]))
        return ctx.invoke(f, *args, **kwargs)

    return update_wrapper(new_func, f)


# Aliases
onlineChain = chain
online = chain
offlineChain = offline
unlockWallet = unlock
