# -*- coding: utf-8 -*-
import json
import click
import datetime
from dxpchain.benefactor import Benefactors
from dxpchain.account import Account
from dxpchain.amount import Amount
from .decorators import onlineChain, unlockWallet
from .main import main, config
from .ui import print_table, print_tx


@main.command()
@click.pass_context
@onlineChain
@click.argument("benefactors", nargs=-1)
@click.option(
    "--account",
    default=config["default_account"],
    help="Account that takes this action",
    type=str,
)
@unlockWallet
def approveBenefactor(ctx, benefactors, account):
    """ Approve benefactor(es)
    """
    print_tx(ctx.dxpchain.approveBenefactor(benefactors, account=account))


@main.command()
@click.pass_context
@onlineChain
@click.argument("benefactors", nargs=-1)
@click.option(
    "--account",
    help="Account that takes this action",
    default=config["default_account"],
    type=str,
)
@unlockWallet
def disapproveBenefactor(ctx, benefactors, account):
    """ Disapprove benefactor(es)
    """
    print_tx(ctx.dxpchain.disapproveBenefactor(benefactors, account=account))


@main.command()
@click.pass_context
@onlineChain
@click.argument("account", default=None, required=False)
@click.option("--top", type=int)
@click.option("--sort", default="total_votes_for")
def benefactors(ctx, account, top, sort):
    """ List all benefactors (of an account)
    """

    def normalize_sort_keys(name):
        if name == "votes":
            return "total_votes_for"
        return name

    benefactors = Benefactors(account)
    t = [["id", "name/url", "daily_pay", "votes", "time", "account"]]
    sort = sort
    sort = normalize_sort_keys(sort)
    if sort in ["total_votes_for"]:
        Benefactors_sorted = sorted(benefactors, key=lambda x: int(x[sort]), reverse=True)
    elif sort == "id":
        Benefactors_sorted = sorted(
            benefactors, key=lambda x: int(x[sort].split(".")[2]), reverse=True
        )
    else:
        Benefactors_sorted = sorted(benefactors, key=lambda x: x[sort], reverse=True)
    if top:
        Benefactors_sorted = Benefactors_sorted[: top + 1]
    for benefactor in Benefactors_sorted:
        if benefactor["work_end_date"] < datetime.datetime.utcnow():
            continue
        votes = Amount({"amount": benefactor["total_votes_for"], "asset_id": "1.3.0"})
        amount = Amount({"amount": benefactor["daily_pay"], "asset_id": "1.3.0"})
        t.append(
            [
                benefactor["id"],
                "{name}\n{url}".format(**benefactor),
                str(amount),
                str(votes),
                "{work_begin_date:%Y-%m-%d}\n-\n{work_end_date:%Y-%m-%d}".format(
                    **benefactor
                ),
                str(Account(benefactor["Benefactor_account"])["name"]),
            ]
        )
    print_table(t)
