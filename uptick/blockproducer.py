import click
from prettytable import PrettyTable
from dxpchain.blockproducer import Blockproducers
from .decorators import onlineChain, unlockWallet
from .main import main, config
from .ui import print_tx, print_table


@main.command()
@click.pass_context
@onlineChain
@click.argument("blockproducers", nargs=-1)
@click.option(
    "--account",
    default=config["default_account"],
    help="Account that takes this action",
    type=str,
)
@unlockWallet
def approveblockproducer(ctx, blockproducers, account):
    """ Approve blockproducer(es)
    """
    print_tx(ctx.dxpchain.approveblockproducer(blockproducers, account=account))


@main.command()
@click.pass_context
@onlineChain
@click.argument("blockproducers", nargs=-1)
@click.option(
    "--account",
    help="Account that takes this action",
    default=config["default_account"],
    type=str,
)
@unlockWallet
def disapproveblockproducer(ctx, blockproducers, account):
    """ Disapprove blockproducer(es)
    """
    print_tx(ctx.dxpchain.disapproveblockproducer(blockproducers, account=account))


@main.command()
@click.pass_context
@onlineChain
def blockproducers(ctx):
    """ List blockproducers and relevant information
    """
    t = [
        [
            "weight",
            "account",
            "signing_key",
            "vote_id",
            "url",
            "total_missed",
            "last_confirmed_block_num",
        ]
    ]
    for blockproducer in sorted(Blockproducers(), key=lambda x: x.weight, reverse=True):
        blockproducer.refresh()
        t.append(
            [
                "{:.2f}%".format(blockproducer.weight * 100),
                blockproducer.account["name"],
                blockproducer["signing_key"],
                blockproducer["vote_id"],
                blockproducer["url"],
                blockproducer["total_missed"],
                blockproducer["last_confirmed_block_num"],
            ]
        )
    print_table(t)
