import click
from .decorators import onlineChain, unlockWallet
from .ui import print_tx
from .main import main, config


@main.command()
@click.pass_context
@onlineChain
@click.argument("members", nargs=-1)
@click.option(
    "--account",
    default=config["default_account"],
    help="Account that takes this action",
    type=str,
)
@unlockWallet
def approvedxpcore(ctx, members, account):
    """ Approve dxpcore member(s)
    """
    print_tx(ctx.dxpchain.approvedxpcore(members, account=account))


@main.command()
@click.pass_context
@onlineChain
@click.argument("members", nargs=-1)
@click.option(
    "--account",
    help="Account that takes this action",
    default=config["default_account"],
    type=str,
)
@unlockWallet
def disapprovedxpcore(ctx, members, account):
    """ Disapprove dxpcore member(s)
    """
    print_tx(ctx.dxpchain.disapprovedxpcore(members, account=account))


@main.command()
@click.pass_context
@onlineChain
@click.argument("url", type=str)
@click.option(
    "--account",
    help="Account that takes this action",
    default=config["default_account"],
    type=str,
)
@unlockWallet
def createdxpcore(ctx, url, account):
    """ Setup a dxpcore account for your account
    """
    print_tx(ctx.dxpchain.create_dxpcore_member(url, account=account))
