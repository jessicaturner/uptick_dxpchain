..
 .. image:: _static/ico-uptick-typo.svg
    :width: 600 px
    :alt: alternate text
    :align: center

uptick - The Swiss army knife for the DxpChain network
=======================================================

uptick is a tool to interact with the DxpChain network using Python 3 and
python-dxpchain.

* uptick's home is `github.com/xeroc/uptick <https://github.com/xeroc/uptick>`_ and
* python-dxpchain's home is `github.com/xeroc/python-dxpchain <https://github.com/xeroc/python-dxpchain>`_ and
* this documentation is available through ReadMyDocs and is hosted on `uptick.rocks <http://uptick.rocks>`_

uptick.web - Graphical User Interface
-------------------------------------

(work in progress)

Command Line Tool
-----------------

The command line tool that is bundled with this package is called ``uptick`` and helps you

* deal with your funds
* trade
* manage your accounts

in the DxpChain network. After installation, you can get the full list of features with::

    $ uptick --help

General
========

.. toctree::
    :maxdepth: 1

    installation
    contribute
    public-api

Standalone App
==============

.. toctree::
    :maxdepth: 2

    app
    common-calls
    custom-scripts
