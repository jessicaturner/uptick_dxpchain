# Uptick

Swiss army knife for interacting with the DxpChain blockchain.

## Documentation

The full-length documentation can be found on [http://uptick.dxperts](http://uptick.dxperts).

## Installation

```
python setup.py install --user
```

## Contributing

uptick welcomes contributions from anyone and everyone. Please
see our [guidelines for contributing](CONTRIBUTING.md) and the [code of
conduct](CODE_OF_CONDUCT.md).

### Discussion and Developers

Discussions around development and use of this library can be found in a
[dedicated Telegram Channel](https://t.me/pydxpchain)

### License

A copy of the license is available in the repository's
[LICENSE](LICENSE.txt) file.
